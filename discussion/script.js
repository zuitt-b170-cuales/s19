// ECMAScript is the standard that is used to create implementations of the language, one of which is JavaScript.

// ES6 updates brings new features

// S19 is to focus on basic updates from ES6
// This allows us to write less and do more.

// Exponent Operator
	// pre-ES6
		const firstNum = Math.pow(8, 2);
		console.log(firstNum);
	// ES6
		const secondNum = 8**2;
		console.log(secondNum);


// Template Literals - allows writing of strings without the use of concat operator;
/*
	MINIACTIVITY
		-create a name variable and  store a name of a person
		-create a message, greeting the person and welcoming him/her in the programming field
		-log in the console the message
*/

	// pre-ES6
		let name = "John";
		let message = "Hello " + name + "! Welcome to the programming field!";
		console.log(message);
	// ES6
		message = `Hello ${name}! Welcome to the programming field!`;
		console.log(message);

// Multiline
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8**2 with the answer of ${secondNum}`;
console.log(anotherMessage);

// Computation inside the template literals
const interestRate = 0.1;
const principal = 1000;
console.log(`The interest on your savings is ${principal*(1+interestRate)}`);

/*
	MINIACTIVITY 2
		-create an array that has firstName, middleName, and lastName elements
		-login the console each element (1 row - 1 element)
*/



// Array Destructuring - allows unpacking elements in arrays into distinct variables; allows naming of array elements with variable instead of using index numbers; helps iwth the code readability and coding efficiency.
/*
	SYNTAX:
		let/const [variableA, variableB, variableC, ... variableN] = arrayName
*/

// pre-ES6
const fullName = ["Juan", "Dela", "Cruz"];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`);

// ES6
const [ firstName, middleName, lastName ] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);



// Object Destructuring
/*
create an object that contains a woman's givenName, maidenName, familyName
log in the console each key (1 row - 1 key-value pair)
*/ 

// pre-ES6
let women = {
	givenName: "Maria",
	maidenName: "Maharlika",
	familyName: "Clara"
};
console.log(women.givenName)
console.log(women.maidenName)
console.log(women.familyName)

// ES6
const {givenName, maidenName, familyName} = women;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);


// Arrow Function
/*
	compact alternative syntax to traditional functions; useful for code snippets where creating functions will be reused in anyo ther parts of the code; Don't repeat yourself - there is no need to create a function that will not be used in the other parts or portions of the code.
*/
/*
	create a function that displays/logs a person's firstName, middleInitial, and lastName in the console
*/
// pre-ES6

function fullie(firstie, middie, lastie){
	console.log(firstie);
	console.log(middie);
	console.log(lastie);
	console.log(firstie, middie, lastie);
};
fullie("Owen", "A", "Cuales");

/*
Parts
	declaration
	functionName
	parameters
	statements
	invoke / call function
*/

// ES6
const printFName=(fname, mname, lname) => {
	console.log(fname, mname, lname);
}

printFName("Will", "D.", "Smith");


// Arrows Functions with loop/methods
/*
	use forEach method to log each student in the console
*/

// pre-ES6
const students = ['John', 'Jane', 'Joe'];
students.forEach(
		function(pick){
			console.log(pick);
		}
	);

// ES6
	// if you have 2 or more parameters, enclose them inside a pair of parenthesis
students.forEach(x => console.log(x))


// (Arrow Functions) Implicit Return Statement - return statement/s are omitted because even without them, JS implicitly adds them for the result of the function
/*
	create a function that adds two numbers using return
*/


// pre-ES6
function addTwo(a, b){
	return a + b;
};

let total = addTwo(1, 2);
console.log(total);


// ES6
const addies = (a, b) => a + b;
// the code above actually runs as const addies = (a, b) => return a + b;
let totallie = addies(4, 6);
console.log(totallie);


// (Arrow Function) Default Function Argument Value
/*
	provides a default argument value if no parameters are included / specified once the function has been invoked
*/

const greet = (name = "User") => {
	return `Good morning, ${name}`
};

console.log(greet());
// once the function has specified parameter value
console.log(`Result of specified value for parameter: ${greet("John")}`);

/*
	create a car object using the object method
	car field: name, brand, year
*/


/*
function car(name, brand, year){
	this.name = name
	this.brand = brand
	this.year = year
};

const car1 = new car("Challenger", "Dodge", "2021");
console.log(car1);*/



//class constructor
// class keyword declares the creation of a "Car" object
class car {
	// constructor keyword = special method of creating or initializing an object for the "car" class
	constructor (brand, name, year){
		// this - sets the properties that refers that are included in the "car" class
		this.brand = brand,
		this.name = name,
		this.year = year
	}
};
const car1 = new car("Challenger", "Dodge", "2021");
console.log(car1);

const car2 = new car();
console.log(car2);
car2.brand = "Toyota",
car2.name = "Fortuner",
car2.year = 2020
console.log(car2);



/*
	create an if-else statement using ternary operator
	condition: if the number is <= 0, return true, if it is >0, return false
*/

let num = 10;
(num <= 0) ? console.log(true): console.log(false);