/*function cube(x){
	return x**3;
	console.log(`The cube is ${this.cube}`)
};

let getCube = cube;*/

const getCube = (number = "x") => {
	return number**3;
};

console.log(getCube(2));
console.log(`The cube of 2 is ${getCube(2)}`)

const address = [261, "Geneva", "Marikina", "Philippines"];
const [streetNumber, street, city, country] = address;
console.log(`You are from ${streetNumber}, ${street}, ${city}, ${country}`);


let animal = {
	name: "Lolong",
	animalType: "Saltwater Crocodile",
	weight: "1075 kg",
	mesaurement: "20ft and 3in"
};

const {name, animalType, weight, mesaurement} = animal;
console.log(`${name} is a ${animalType}. He weighed ${weight} with a measurement of ${mesaurement}`);

const numberArray = [1, 2, 3, 4, 5];
numberArray.forEach(x => console.log(x));

const reduceNumber = numberArray.reduce(function (a, b){
	return a + b;
}, 0);

console.log(reduceNumber);

class Dog {
	constructor (name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
};

const dog1 = new Dog("Frankie", 5, "Miniature Daschund");

console.log(dog1);